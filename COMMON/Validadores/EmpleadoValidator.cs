﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class EmpleadoValidator: GenericValidator<Empleado>
    {
        public EmpleadoValidator() 
        {
            RuleFor(g => g.Nombre).NotEmpty().MaximumLength(50);
            RuleFor(g => g.ApellidoP).NotEmpty().MaximumLength(50);
            RuleFor(g => g.ApellidoM).NotEmpty().MaximumLength(50);
            RuleFor(g => g.FechaNacimiento).NotEmpty();
            RuleFor(g => g.Telefono).NotEmpty();
            RuleFor(g => g.Correo).NotEmpty();
        }
    }
}
