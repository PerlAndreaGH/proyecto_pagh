﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Venta : Base
    {
        public DateTime FechaVenta { get; set;  }
        public string IdEmpleado { get; set; }
        public string IdProducto { get; set; }

    }
}
