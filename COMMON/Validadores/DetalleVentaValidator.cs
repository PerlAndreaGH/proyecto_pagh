﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class DetalleVentaValidator: GenericValidator<DetalleVenta>
    {
        public DetalleVentaValidator() 
        {
            RuleFor(g => g.IdVenta).NotEmpty();
            RuleFor(g => g.IdProducto).NotEmpty();
            RuleFor(g => g. Cantidad).NotEmpty();
            RuleFor(g => g.Precio).NotEmpty();
        }
    }
}
