﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class DetalleVenta : Base
    {
        public string IdVenta { get; set; }
        public string IdProducto { get; set; }
        public int Cantidad { get; set;  }
        public int Precio { get; set; }
        public string IdUsuario { get; set; }
        
    }
}
