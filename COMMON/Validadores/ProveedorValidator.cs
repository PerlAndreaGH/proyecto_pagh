﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class ProveedorValidator: GenericValidator<Proveedor>
    {
        public ProveedorValidator() 
        {
            RuleFor(g => g.Nombre).NotEmpty().MaximumLength(50);
            RuleFor(g => g.ApellidoP).NotEmpty().MaximumLength(50);
            RuleFor(g => g.ApellidoM).NotEmpty().MaximumLength(50);
            RuleFor(g => g.Telefono).NotEmpty().MaximumLength(50);
            RuleFor(g => g.Email).NotEmpty().MaximumLength(50);
        } 
    }
}
