﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Proveedor : Base
    {
        public string Nombre { get; set; }
        public string  ApellidoP { get; set; }
        public string ApellidoM { get; set;  }
        public string Telefono { get; set; }
        public string Email { get; set; }

        public override string ToString ()
        {
            return Nombre;
        }
    }
}
