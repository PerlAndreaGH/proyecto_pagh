﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores; 

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : GenericController<Categoria>
    {
        public CategoriaController() : base(new CategoriaValidator()) 
        {
        }
    }
}
