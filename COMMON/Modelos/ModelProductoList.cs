﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;

namespace COMMON.Modelos
{
    public class ModelProductoList: Producto
    {
        public string Imagen { get; set; }
        public string NombreProveedor { get; set; }
        public string NombreCategoria { get; set; } 
    }
}
