﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades; 
using FluentValidation;

namespace COMMON.Validadores
{
    public class CategoriaValidator:GenericValidator<Categoria>
    {
        public CategoriaValidator() 
        {
            RuleFor(c => c.NombreCategoria).NotEmpty(). MaximumLength(100);

        }
    }
}
