﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoController : GenericController<Empleado>
    {
        public EmpleadoController() : base(new EmpleadoValidator()) 
        {
        }
    }
}
