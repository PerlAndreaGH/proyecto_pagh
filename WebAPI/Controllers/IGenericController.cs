﻿using COMMON.Entidades;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public interface IGenericController<T> where T : Base
    {
        ActionResult<bool> Delete(string id);
        ActionResult<List<T>> Get();
        ActionResult<T> Get(string id);
        ActionResult<T> Post([FromBody] T value);
        ActionResult<T> Put(string id, [FromBody] T value);
    }
}