﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Categoria : Base 
    {
       public  string NombreCategoria { get; set; }
        public override string ToString () 
        {
            return NombreCategoria;
        }
    }
}
