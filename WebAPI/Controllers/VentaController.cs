﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentaController : GenericController<Venta>
    {
        public VentaController() : base( new VentaValidator()) 
        {
        }
    }
}
