﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class VentaValidator: GenericValidator<Venta>
    {
        public VentaValidator() 
        {
            RuleFor(g => g.FechaVenta).NotEmpty();
            RuleFor(g => g.IdEmpleado).NotEmpty();
            RuleFor(g => g.IdProducto).NotEmpty();
        }
    }
}
