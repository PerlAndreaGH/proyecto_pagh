﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class ProductoValidator: GenericValidator<Producto>
    {
        public ProductoValidator()
        {
            RuleFor(g => g.IdCategoria).NotEmpty();
            RuleFor(g => g.NombreProducto).NotEmpty().MaximumLength(50);
            RuleFor(g => g.Precio).NotEmpty();
            RuleFor(g => g.Existencia).NotEmpty();
            RuleFor(g => g.PrecioUnitario).NotEmpty();
            RuleFor(g => g.PrecioVenta).NotEmpty();
            RuleFor(g => g.IdProveedor).NotEmpty();
        }
    }
}
