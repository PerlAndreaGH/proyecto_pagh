﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Producto : Base
    {
        public string IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public double Precio { get; set; }
        public int Existencia { get; set; }
        public int PrecioUnitario { get; set; }
        public int PrecioVenta { get; set; }
        public string IdProveedor { get; set; }

        public override string ToString()
        {
            return $"{IdProducto} - {NombreProducto} - Disponiblr: {Existencia}";
        }
    }
}
