﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : GenericController<Producto>
    {
        public ProductoController() : base( new ProductoValidator()) 
        {
        } 
    }
}
